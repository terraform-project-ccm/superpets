import React, { useState } from 'react';
import { Transition} from 'react-transition-group';
import classNames from 'classnames';
import styles from '../Interact.module.css';
import Button from '@mui/material/Button';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import SwipeVerticalIcon from '@mui/icons-material/SwipeVertical';
import HealthAndSafetyIcon from '@mui/icons-material/HealthAndSafety';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import PetsContext from '../contexts/PetsContext'
import SportsBaseballIcon from '@mui/icons-material/SportsBaseball';
import { Alert, AlertTitle } from '@mui/material';
import { popUpDefaultStyles, transitionStyles, Modal } from '../styling/InteractStyling.js';

function InteractContent() {
  const [animate, setAnimate] = useState(false);
  const handleClick = () => setAnimate(!animate);

  const [showPetSuccess, setShowPetSuccess] = useState('hidden');
  const handleShowPetSuccess = (successStatus) => setShowPetSuccess(successStatus);

  const [disable, setDisableSearch] = useState(false);
  const [disablePet, setDisablePet] = useState(true);

  const [showModal, setShowModal] = useState(false);
  const [showModalMeds, setShowModalMeds] = useState(false);
  const [showModalPlay, setShowModalPlay] = useState(false);
  const handleShowModalClick = () => setShowModal(!showModal);
  const handleShowModalClickMeds = () => setShowModalMeds(!showModalMeds);
  const handleShowModalClickPlay = () => setShowModalPlay(!showModalPlay);

  const [state, ] = React.useContext(PetsContext);

  const [dogInteractName, setDogInteractName] = useState('There are many amazing super pets looking for some attention.')
  const [dogPic, setDogPic] = useState('')
  const [dogNumber, setDogNumber] = useState(0)

  const [barStatus, setBarStatus] = useState(5);
  const handleFill = () => {
    if (barStatus >= 95) {
      handleShowPetSuccess('visible')
      setDisableSearch(false)
      setDisablePet(true)
    }
    setBarStatus(barStatus + 5);
  }

  function handleLoad() {
    fetch(`${state.url}/animals?type=dog&location=85138&distance=60`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application.json',
        'Authorization': 'Bearer ' + state.token
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.animals[dogNumber].photos.length > 0) {
        setDogInteractName(data.animals[dogNumber].name + ' is in need of a cuddle session!')
        setDogPic(<Card sx={{ maxWidth: 345, display: 'inline-block', margin: 2 }}><CardMedia
          component="img"
          width="200px"
          height="200px"
          image={data.animals[dogNumber].photos[0].full}
          alt="No Photo Provided :("
        /></Card>)
        setDisableSearch(true);
        setBarStatus(5);
        handleShowPetSuccess('hidden');
        setDisablePet(false)
      } 
        setDogNumber(dogNumber + 1)
      })
  };


  return (
    <>
      <h3>{dogInteractName}</h3>
        <Button disabled={disable} onClick={handleLoad}>
          <h4>Search For A Pet</h4>
        </Button>
        <p>{dogPic}</p>
        <Button disabled={disablePet} variant="contained" endIcon={<SwipeVerticalIcon />} onClick={() => { handleClick(); handleFill() }} className={classNames(
          styles.animate,
          animate && styles.grow
        )}>
          Give Scritches
        </Button>
        <p><progress value={barStatus} max="100" /></p>
        <Alert sx={{ visibility: showPetSuccess }} severity="success">
          <AlertTitle>Horray!</AlertTitle>
          You did it — <strong>that is one happy Super Pet!</strong>
        </Alert>

      <div className={styles.container}>
        <h4>Pet Needs:</h4>
        <Card sx={{ maxWidth: 300, height: 400, display: 'inline-block', margin: 2 }}>
          <CardContent>
            <Button variant="contained" startIcon={<FastfoodIcon />} onClick={handleShowModalClick}>"Want a Treat?"</Button>
            <Transition in={showModal} timeout={300}>
              {transitionState => (
                <Modal
                  title="Give a treat to a Super Pet, they could use a snack!"
                  text="Click here to donate a bag of food to SuperPets."
                  styles={{
                    ...popUpDefaultStyles,
                    ...transitionStyles[transitionState]
                  }}
                  onClose={handleShowModalClick} />
              )}
            </Transition>
          </CardContent>
        </Card>

        <Card sx={{ maxWidth: 300, height: 400, display: 'inline-block', margin: 2 }}>
          <CardContent>
            <Button variant="contained" startIcon={<HealthAndSafetyIcon />} onClick={handleShowModalClickMeds}>"Feeling Okay?"</Button>
            <Transition in={showModalMeds} timeout={300}>
              {state => (
                <Modal
                  title="Help out a Super Pet that is in need of vet work!"
                  text="Click here to donate vet medication to SuperPets."
                  styles={{
                    ...popUpDefaultStyles,
                    ...transitionStyles[state]
                  }}
                  onClose={handleShowModalClickMeds} />
              )}
            </Transition>
          </CardContent>
        </Card>

        <Card sx={{ maxWidth: 300, height: 400, display: 'inline-block', margin: 2 }}>
          <CardContent>
            <Button variant="contained" startIcon={<SportsBaseballIcon />} onClick={handleShowModalClickPlay}>"Want to Fetch?"</Button>
            <Transition in={showModalPlay} timeout={300}>
              {state => (
                <Modal
                  title="Ooo, a Super Pet looks like they are in the mood to play!"
                  text="Click here to donate a toy to SuperPets."
                  styles={{
                    ...popUpDefaultStyles,
                    ...transitionStyles[state]
                  }}
                  onClose={handleShowModalClickPlay} />
              )}
            </Transition>
          </CardContent>
        </Card>

      </div>
    </>
  );
}

export default InteractContent