import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Cat from '../imgs/Cat.jpg';
import Dog from '../imgs/Dog.jpg';
import Rabbit from '../imgs/Rabbit.jpg';
import { Link } from 'react-router-dom';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const Div = styled('div')(({ theme }) => ({
  ...theme.typography.button,
  backgroundColor: '#eaeff1',
  padding: theme.spacing(1),
  textAlign: 'center',
  fontSize: 18,
  color: '#009be5'
}));

const questions = [
  {
    id: 1,
    what: 'How do you prefer to spend your time?',
    answers: [
      'Walking, Running, or Biking',
      'Watching TV or Reading',
      'Spending Time With Friends'
    ]
  },
  {
    id: 2,
    what: 'How would you describe your home?',
    answers: [
      'Messy - Too Busy to Organize',
      'Warm and Cozy',
      'Perfect for Organizing'
    ]
  },
  {
    id: 3,
    what: 'Where do you prefer to live?',
    answers: [
      'Off the Grid',
      'Quiet Suburbs',
      'Bustling City'
    ]
  },
  {
    id: 4,
    what: 'Which word best describes you?',
    answers: [
      'Active',
      'Relaxed',
      'Social'
    ]
  },
  {
    id: 5,
    what: 'What is your work situation for the forseeable future?',
    answers: [
      'Consistently On the Go',
      'Working from Home',
      'Corporate Office'
    ]
  },
  {
    id: 6,
    what: 'Your dream vacation is...',
    answers: [
      'Something Adventurous',
      'An Island Escape',
      'Exploring New Cities'
    ]
  },
  {
    id: 7,
    what: 'Which describes your styles?',
    answers: [
      'Atheletic',
      'Casual',
      'Unique'
    ]
  }
]

const suggestedPet = {
  active: ['Cat', 'cat or another low-maintanence animal', Cat],
  relaxed: ['Rabbit', 'rabbit or another animal you would enjoy as company', Rabbit],
  social: ['Dog', 'dog or another animal that would complement how you already spend your time', Dog]
}

function PersonalityQuizContent() {
  const [state, setState] = React.useState({
    currentQuestion: 0,
    answers: [],
    isDone: false,
    winner: ''
  });

  const handleSelect = (e) => {
    setState({
      ...state,
      answers: [...state.answers, questions[state.currentQuestion].answers.indexOf(e.target.innerText)],
      currentQuestion: state.currentQuestion + 1,
      isDone: questions[state.currentQuestion].id === 7 ? true : false
    });
  }

  const determineWinner = () => {
    const results = [
      ['active', state.answers.filter(x => x === 0).length],
      ['relaxed', state.answers.filter(x => x === 1).length],
      ['social', state.answers.filter(x => x === 2).length]
    ]

    results.sort(function (a, b) {
      return a[1] - b[1];
    });

    return results[results.length - 1][0];
  }

  if (state.isDone && !state.winner) {
    setState({
      ...state,
      winner: determineWinner()
    });
  }

  return (
    <div>
      <Div>
        {
          !state.isDone &&
          questions[state.currentQuestion].what
        }
      </Div>
      < Box sx={{ width: '100%' }}>
        <Stack spacing={1} >
          {
            !state.isDone &&
            questions[state.currentQuestion].answers.map(ans => <Item
              key={questions[state.currentQuestion].answers.indexOf(ans)}
              onClick={(e) => handleSelect(e)}>
              {ans}
            </Item>)
          }
        </Stack>
      </Box>
      <Box sx={{ width: '100%' }}>
        {
          state.winner &&
          (
            <Card sx={{ maxWidth: 345, margin: 'auto' }}>
              <CardMedia
                component="img"
                height="140"
                image={suggestedPet[state.winner][2]}
                alt="ideal pet"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {suggestedPet[state.winner][0]}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  Based on your {state.winner} personality, a {suggestedPet[state.winner][1]} would be an ideal pet for you!
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" component={Link} to={'/pets'}>Find a friend!</Button>
              </CardActions>
            </Card>
          )
        }
      </Box>
    </div >
  )
}

export default PersonalityQuizContent