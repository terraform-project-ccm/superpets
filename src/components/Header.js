import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import TabPanel from '@mui/lab/TabPanel';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Box from '@mui/material/Box';
import spLogo from '../imgs/petsLogo.png'

// const lightColor = 'rgba(255, 255, 255, 0.7)';
const Div = styled('div')(({ theme }) => ({
  ...theme.typography.button,
  padding: theme.spacing(1),
  textAlign: 'center',
  fontSize: 18,
  color: '#009be5',
  backgroundColor: '#009be5'
}));

function Header(props) {
  const [pageContent, setPageContent] = React.useState('0');

  const handleChange = (event, newValue) => {
    setPageContent(newValue);
  };

  return (
    <React.Fragment>
      <TabContext value={pageContent}>

        <AppBar
          component="div"
          color="primary"
          position="static"
          elevation={0}
          sx={{ zIndex: 0 }}
        >
          <Div></Div>
          <Toolbar>
            <Grid container alignItems="center" spacing={1}>
              <Grid item xs>
                <Typography color="inherit" variant="h5" component="h1">
                  {props.title}
                </Typography>
              </Grid>
              <Grid item alignItems="center">
                <img src={spLogo} alt="logo" width='100' padding="50px" />
              </Grid>
            </Grid>
          </Toolbar>

        </AppBar>
        <AppBar component="div" position="static" elevation={0} sx={{ zIndex: 0 }}>
          <TabList textColor="inherit" onChange={handleChange}>
            {
              props.tabs.map(tab => <Tab key={props.tabs.indexOf(tab)} label={tab.title} value={props.tabs.indexOf(tab) + ''} />)
            }
          </TabList>

        </AppBar>
        <Box component="main" sx={{ flex: 1, py: 6, px: 4, bgcolor: '#eaeff1' }}>
          {
            props.tabs.map(tab => {
              return (
                <TabPanel key={props.tabs.indexOf(tab)} value={props.tabs.indexOf(tab) + ''}>
                  {tab.comp}
                </TabPanel>
              )
            })
          }

        </Box>
      </TabContext>

    </React.Fragment >
  );
}

export default Header;