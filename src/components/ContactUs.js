import React, { useState } from 'react';
import { Box, Card, Typography, TextField, Button } from '@mui/material';
function ContactUs() {
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [submitted, setSubmitted] = useState(false);
  const submitForm = (e) => {
    setSubmitted(true);
    e.preventDefault();
  };
  if (submitted) {
    return (
      <Box>
        <Card>
          <Typography variant='h5'>Thank you!</Typography>
          <Typography variant='h5'>We'll be in touch soon.</Typography>
        </Card>
      </Box>
    )
  }
  return (
    <Box>
      <Typography variant="h4">
        Contact Us
      </Typography>
      <Typography
        component="form"
        noValidate
        autoComplete="off"
      >
        <TextField
          label="Full Name"
          variant="outlined"
          fullWidth
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
        <TextField
          label="Email"
          variant="outlined"
          fullWidth
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          label="Subject"
          variant="outlined"
          fullWidth
          value={subject}
          onChange={(e) => setSubject(e.target.value)}
        />
        <TextField
          label="Enter a message"
          variant="outlined"
          fullWidth
          multiline={true}
          value={message}
          onChange={(e) => setMessage(e.target.value)}>
        </TextField>
        <Button
          variant="contained"
          type="submit"
          color="primary"
          sx={{ width: '150px', fontSize: '16px' }}
          onClick={submitForm}
        > Submit
        </Button>
      </Typography>
    </Box>
  );
};
export default ContactUs
