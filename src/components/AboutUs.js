import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

function AboutUs() {
  return (
    <div>
      <div>
        <Typography variant='h4' mb='15px' ml='10px'>Meet the SuperPets Team!</Typography>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant='h6'>Cindy</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Cindy loves animals and owns one dog, Duke.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography variant='h6'>Greg</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Greg is awesome and fosters many dogs.
            </Typography>
          </AccordionDetails>
        </Accordion>

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography variant='h6'>Miranda</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Miranda's cats love helping her code.
            </Typography>
          </AccordionDetails>
        </Accordion>

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography variant='h6'>Tony</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Tony will be adopting a dog after working on this project.
            </Typography>
          </AccordionDetails>
        </Accordion>
        
      </div>

    </div>
  )
}

export default AboutUs