import React from 'react'
import PetsContext from '../contexts/PetsContext';
import { ImageList, ImageListItem, Box } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';

const Div = styled('div')(({ theme }) => ({
  ...theme.typography.button,
  backgroundColor: '#eaeff1',
  padding: theme.spacing(1),
  textAlign: 'center',
  fontSize: 18,
  color: '#009be5'
}));

function getRandomPageNumber(pageCount) {
  let min = Math.ceil(1);
  let max = Math.floor(pageCount);
  return Math.floor(Math.random() * (max - min) + min);
}

function Home() {
  const [state, dispatch] = React.useContext(PetsContext);

  // function getAllPets(pageNumber) {
  //   state.loaded = true;
  //   fetch(
  //     `${state.url}/animals?page=${pageNumber}`, {
  //     method: 'GET',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer ' + state.token
  //     }
  //   })
  //     .then(res => res.json())
  //     .then(data => {
  //       dispatch({ type: 'find_all_animals', payload: data.animals });
  //     })

  // }

  React.useEffect(() => {
    if (state.token && !state.loaded) {
      const rndPage = getRandomPageNumber(1234);
      state.loaded = true;
      fetch(
        `${state.url}/animals?page=${rndPage}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + state.token
        }
      })
        .then(res => res.json())
        .then(data => {
          dispatch({ type: 'find_all_animals', payload: data.animals });
        });
    }
  }, [state, dispatch]);

  return (
    <>
      <Div>Thousands of Super Pets are looking for a Sidekick in YOU!
        <p><Link to='/pets' style={{textDecoration: 'none', color: '#009be5'}}>Find a new companion today!</Link></p>
      </Div>
      <Box sx={{ border: 'solid 5px', color: '#009be5', width: '75%', height: 600, overflowY: 'scroll', margin: 'auto' }}>
        <ImageList sx={{ padding: '8px' }} variant="masonry" cols={3} gap={8}>
          {state.allAnimals.map(pet => {
            return (
              pet.photos.length > 0 &&
              <ImageListItem key={pet.id}>
                <img
                  src={`${pet.photos[0].full}?w=50&fit=crop&auto=format`}
                  srcSet={`${pet.photos[0].full}?w=50&fit=crop&auto=format&dpr=2 2x`}
                  alt={pet.name}
                  loading="lazy"
                />
              </ImageListItem>
            )
          })
          }
        </ImageList>
      </Box>
    </>
  )
}
export default Home;