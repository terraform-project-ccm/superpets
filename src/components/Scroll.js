import React, { useState, useEffect } from 'react';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import { IconButton } from '@mui/material';

function Scroll({ showBelow }) {
    const [show, setShow] = useState(showBelow ? false : true);

    const handleScroll = () => {
        if (window.pageYOffset > showBelow) {
            if (!show) { setShow(true); }
        } else {
            if (show) setShow(false);
        }
    }

    useEffect(() => {
        if (showBelow) {
            window.addEventListener('scroll', handleScroll);
            return () => window.removeEventListener('scroll', handleScroll);
        }
    });

    const handleClick = () => {
        window['scrollTo']({ top: 0, behavior: 'smooth' });
    }

    return (
        <div>
            {
                show &&
                <IconButton
                    onClick={handleClick}
                    sx={{
                        zIndex: 2,
                        position: 'fixed',
                        bottom: '2vh',
                        backgroundColor: '#DCDCDC',
                        color: 'black',
                        "&:hover, &.Mui-focusVisible": {
                            transition: '0.3s',
                            color: '#397BA6',
                            backgroundColor: '#DCDCDC'
                        },
                        right: '5%'
                    }}
                >
                    <ExpandLessIcon />
                </IconButton>
            }
        </div>
    )
};

export default Scroll;