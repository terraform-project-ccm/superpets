import React, { useState, useContext } from 'react';
import PetsContext from '../contexts/PetsContext';
import altImg from '../imgs/spPlaceholder_img.png';

import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import BackspaceIcon from '@mui/icons-material/Backspace';
import { Backdrop } from '@mui/material';
import { CircularProgress } from '@mui/material';

function SearchOrgsContent() {

  const [state, dispatch] = useContext(PetsContext);
  const [distance, setDistance] = useState('10');
  const [open, setOpen] = React.useState(false);
  const [location, setLocation] = useState("");

  function handleClick(location) {
    if (!location) { location = 85138 };
    fetch(`${state.url}/organizations?location=${location}&distance=${distance}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application.json',
        'Authorization': 'Bearer ' + state.token
      }
    })
      .then(res => res.json())
      .then(data => dispatch({ type: 'find_orgs', payload: data.organizations }))
      .then(() => setOpen(false))
  }

  function handleClear() {
    dispatch({ type: 'clear_orgs' })
  }

  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Paper sx={{ maxWidth: 750, margin: 'auto', overflow: 'hidden' }}>
        <Box>
          <Typography variant="body1" color="text.primary" margin="10px">Filter By:</Typography>
          <TextField sx={{ m: 1, minWidth: 250 }} type="search" id="location" label="Zip Code..." onChange={e => setLocation(e.target.value)}></TextField>
          <FormControl sx={{ m: 1, minWidth: 250 }}>
            <InputLabel id="distance-label">Distance</InputLabel>
            <Select
              label="Distance"
              labelId="distance-label"
              id="distance"
              value={distance}
              onChange={e => { setDistance(e.target.value) }}
            >
              <MenuItem value='10'>10 Miles</MenuItem>
              <MenuItem value='25'>25 Miles</MenuItem>
              <MenuItem value='50'>50 Miles</MenuItem>
              <MenuItem value='100'>100 Miles</MenuItem>
            </Select>
          </FormControl  >
          <Button sx={{ margin: '10px' }} size="large" variant="contained" endIcon={<SearchIcon />} onClick={() => {handleClick(location); handleToggle()}}>Search</Button>
          <IconButton variant="contained" onClick={() => handleClear()}><BackspaceIcon /></IconButton>
        </Box>
      </Paper>

      <Box sx={{ margin: 'auto', justifyContent: 'center', alignItems: 'center' }}>
        {state.organizations.map(org => {
          return (
            <Card key={org.id} sx={{ maxWidth: 345, display: 'inline-block', position: 'relative', left: '7.5%', margin: 2 }}>
              <CardMedia
                component="img"
                width="400px"
                height="400px"
                image={org.photos.length > 0 ? org.photos[0].full : altImg}
                alt="No Picture Available :("
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {org.name}
                </Typography>
                <Typography variant="body2" color="text.secondary" sx={{ height: 100, width: 300 }}>
                  {org.mission_statement}
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" target='_blank' href={org.url}>Learn More</Button>
              </CardActions>
            </Card>)
        })}
      </Box>
    </>
  )
}

export default SearchOrgsContent