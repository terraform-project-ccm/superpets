import React from 'react';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

function Copyright() {
  return (
        <Typography variant="body2" color="text.secondary" align="center">
          {'Copyright © '}
          <Link color="inherit" href="https://gitlab.com/stfa-ase-0422/exercises/react-project/sf-ase-superpets">
            SuperPets
          </Link>{' '}
          {new Date().getFullYear()}.
        </Typography>
  );
}

export default Copyright