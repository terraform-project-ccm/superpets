import * as React from 'react';
import { Routes, Route } from 'react-router-dom';

import PetsContext from './contexts/PetsContext';
import Navigator from './components/Navigator';
import Home from './pages/Home';
import Scroll from './components/Scroll';
import Copyright from './components/Copyright';
import FindPet from './pages/FindPet';
import PersonalityQuiz from './pages/PersonalityQuiz';
import Interact from './pages/Interact';
import SearchOrgs from './pages/SearchOrgs';

import { theme } from './styling/AppStyling';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import { blue, deepPurple, teal, green } from '@mui/material/colors';

const initialState = {
  url: 'https://api.petfinder.com/v2/',
  token: '',
  loaded: false,
  dogs: [],
  cats: [],
  rabbits: [],
  birds: [],
  allAnimals: [],
  organizations: [],
  pagination: []
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'fetch_token':
      return { ...state, token: action.payload };
    case 'find_dogs':
      return { ...state, dogs: action.payload };
    case 'find_cats':
      return { ...state, cats: action.payload };
    case 'find_rabbits':
      return { ...state, rabbits: action.payload };
    case 'find_birds':
      return { ...state, birds: action.payload };
    case 'find_all_animals':
      return { ...state, allAnimals: action.payload };
    case 'find_orgs':
      return { ...state, organizations: action.payload };
    case 'clear_dogs':
      return { ...state, dogs: [] };
    case 'clear_cats':
      return { ...state, cats: [] };
    case 'clear_rabbits':
      return { ...state, rabbits: [] };
    case 'clear_other':
      return { ...state, birds: [] };
    case 'clear_orgs':
      return { ...state, organizations: [] };
    case 'pagination':
      return {...state, pagination: action.payload}
    default:
      return { ...state };
  }
}

export default function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    fetch("https://api.petfinder.com/v2/oauth2/token", {
      body: "grant_type=client_credentials&client_id=Kq5s7H1hg4ygEGTL63jqOqSmZfATMu5zga9PqGjKQAZDjE1yIc&client_secret=a7SbtgerMuZB7ZymlOUzTMKITGnaNAHd8gAjFv4n",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: "POST"
    })
      .then(response => response.json())
      .then(data => {
        dispatch({ type: 'fetch_token', payload: data.access_token });
      })
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ display: 'flex', minHeight: '100vh' }}>
        <CssBaseline />
        <Box component="nav" sx={{ width: { sm: 256 }, flexShrink: { sm: 0 } }}>
          <Navigator PaperProps={{ style: { width: 256 } }} sx={{ display: { sm: 'block', xs: 'none' } }} />
        </Box>
        <PetsContext.Provider value={[state, dispatch]}>
          <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
            <Routes>
              <Route exact path='/' element={<Home />} />
              <Route path='/pets' element={<FindPet />} />
              <Route path='/quiz' element={<PersonalityQuiz />} />
              <Route path='/interact' element={<Interact />} />
              <Route path='/search' element={<SearchOrgs />} />
            </Routes>
            <Box component="footer" sx={{ p: 2, bgcolor: '#eaeff1' }}>
              <Scroll showBelow={250} />
            </Box>
            <Box component="footer" sx={{ p: 2, bgcolor: '#eaeff1' }}>
              <Stack direction="row" spacing={.5} justifyContent="center">
                <Avatar sx={{ bgcolor: blue[500] }}>GK</Avatar>
                <Avatar sx={{ bgcolor: green[500] }}>TC</Avatar>
                <Avatar sx={{ bgcolor: teal[500] }}>CR</Avatar>
                <Avatar sx={{ bgcolor: deepPurple[300] }}>MH</Avatar>
              </Stack>
              <Copyright />
            </Box>
          </Box>
        </PetsContext.Provider>
      </Box>
    </ThemeProvider>
  );
}