import React from 'react'
import classNames from 'classnames';
import styles from '../Interact.module.css';


export const popUpDefaultStyles = {
    transition: `opacity 300ms ease-in-out`,
    opacity: 0,
};

export const transitionStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
};

export const Modal = props => (
  <div className={styles.modal} style={props.styles}>
    <div className={styles.modalContent}>
      {props.title && <h4 className={styles.h4}>{props.title}</h4>}
      <p>{props.text}</p>
      <button
        className={classNames(styles.btn, styles.primary)}
        onClick={props.onClose}>
        Close
      </button>
    </div>
  </div>
);

