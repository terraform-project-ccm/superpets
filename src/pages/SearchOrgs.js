import React from 'react'
import Header from '../components/Header';
import SearchOrgsContent from '../components/SearchOrgsContent';

const tabs = [
  {
    title: 'Search',
    comp: <SearchOrgsContent />
  }
]

function SearchOrgs() {
  return (
    <>
      <Header tabs={tabs} title={"Search Adoption Organizations"} />
    </>
  )
}

export default SearchOrgs