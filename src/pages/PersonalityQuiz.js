import React from 'react'
import Header from '../components/Header';
import PersonalityQuizContent from '../components/PersonalityQuizContent';

function PersonalityQuiz() {
  const tabs = [
    {
      title: '',
      comp: <PersonalityQuizContent />
    }
  ]

  return (
    <>
      <Header tabs={tabs} title={"Personality Quiz"} />
    </>
  )
}

export default PersonalityQuiz