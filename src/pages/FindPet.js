import React from 'react';
import Header from '../components/Header';
import Dogs from '../components/Dogs';
import Cats from '../components/Cats';
import Rabbits from '../components/Rabbits';
import OtherAnimals from '../components/OtherAnimals';

const tabs = [
    {
        title: "Dogs",
        comp: <Dogs />
    },
    {
        title: "Cats",
        comp: <Cats />
    },
    {
        title: "Rabbits",
        comp: <Rabbits />
    },
    {
        title: "Other",
        comp: <OtherAnimals />
    },
]

function FindPet() {
    return (
        <>
            <Header tabs={tabs} title={"Find A Pet"} />
        </>
    );
}

export default FindPet;