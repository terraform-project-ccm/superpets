import React from 'react';
import Header from '../components/Header';
import AboutUs from '../components/AboutUs';
import ContactUs from '../components/ContactUs';
import HomePage from '../components/HomePage';

const tabs = [
  {
    title: "Home",
    comp: <HomePage />
  },
  {
    title: "About Us",
    comp: <AboutUs />
  },
  {
    title: "Contact Us",
    comp: <ContactUs />
  }
]

function Home() {
  return (
    <>
      <Header tabs={tabs} title={"Welcome to SuperPets!"} />
    </>
  );
}

export default Home;