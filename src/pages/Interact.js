import React from 'react'
import Header from '../components/Header';
import InteractContent from '../components/InteractContent';


const tabs = [
  {
    title: 'Interact',
    comp: <InteractContent />
  }
]

function Interact() {
  return (
    <>
      <Header tabs={tabs} title={"Interact with Pet"} />
    </>
  )
}

export default Interact